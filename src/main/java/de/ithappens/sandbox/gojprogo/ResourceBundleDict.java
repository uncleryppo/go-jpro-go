package de.ithappens.sandbox.gojprogo;

import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;

/**
 * @author riddler
 */
public class ResourceBundleDict {
    
    private final static String BUNDLE_NAME = "de.ithappens.sandbox.langbundle";
    private final static ResourceBundle RB = ResourceBundle.getBundle(BUNDLE_NAME);
    
    private final static String FXML_NAME = "de/ithappens/sandbox/ui-layout";
    
    public final static String CLICK_ME = get("CLICK_ME");
    public final static String APPLICATION_NAME = get("APPLICATION_NAME");
    
    private static String get(String key) {
        String value = null;
        try {
            value = RB.getString(key);
        } catch (Exception ex) {
            ex.printStackTrace();
            value = "<TEXT_ERROR>";
        } finally {
            if (value == null || value.equals("")) {
                value = key;
            }
            return StringEscapeUtils.unescapeJava(value);
        }
    }

    public static FXMLLoader getFXMLLoader() throws IOException {
        ClassLoader classLoader = ResourceBundleDict.class.getClassLoader();
        String bundleFilename = StringUtils.replace(BUNDLE_NAME, ".", "/") + ".properties";
        return new FXMLLoader(
                classLoader.getResource(FXML_NAME),
                new PropertyResourceBundle(classLoader.getResource(bundleFilename).openStream()));
    }
    
}
