package de.ithappens.sandbox.gojprogo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;

public class GoJproGo extends Application {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            //init
            String title = ResourceBundleDict.APPLICATION_NAME;
            
            //view
            setUserAgentStylesheet(STYLESHEET_MODENA);
            FXMLLoader guiLoader = ResourceBundleDict.getFXMLLoader();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("Go Jpro Go!!!");
    }
}
